Following are the instructions to setup:
1. clone the repo
    git clone git@bitbucket.org:choudharyvarsha59/cubito.git

2. Install the required libraries and packages. On Ubuntu.
    pip install -r requirments.txt

3. start service:
    python run.py

4. hit base url from browser:
   http://127.0.0.1:5000/

5. If you get ratelimited from git then please create one git api key and add it into settings.py (as github has very less ratelimit for unauthenticated requests)
