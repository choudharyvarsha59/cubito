from flask import render_template, jsonify, abort, request, send_file
from app import app
import json
import requests

@app.route('/issues/<state>',methods=['GET'])
def add_buddy(state):
	request_data=request.args
	API_KEY = app.config.get('GITHUB_API_KEY')
	headers = {}
	if API_KEY:
		headers = {"Authorization": "bearer " + API_KEY}
	url = "https://api.github.com/repos"
	github_url = request_data.get('url')
	page = request_data.get("page")
	if not github_url:
		abort(400, "url missing")
	url_parts = github_url.split('github.com')
	url = url + url_parts[1] + '/issues?per_page=5&state=' + state + '&page=' + str(page)
	response = requests.get(url, headers=headers)
	if response.status_code == 404:
		abort(400, "please add a valid public repository url. Like: https://github.com/pythonindia/junction")
	elif response.status_code != 200:
		abort(response.status_code, json.loads(response.content).get('message'))
	issues = json.loads(response.content)
	issues_details = []
	for issue in issues:
		temp = {}
		temp['title'] = issue['title']
		if state == 'open':
			temp['opened_by'] = issue['user']['login']
			temp['opened_at'] = issue['created_at']
		else:
			temp['closed_at'] = issue['closed_at']
			issue_url = issue['url']
			reponse = requests.get(issue_url, headers=headers)
			if response.status_code != 200:
				abort(response.status_code, json.loads(response.content).get('message'))
			issue_body = json.loads(reponse.content)
			temp['closed_by'] = issue_body['closed_by']['login']
		issues_details.append(temp)
	return jsonify(issues_details)

@app.route('/', methods=['GET'])
def home():
	return render_template('base.html', title="")
