
var github_repo_url = ''
var pagestate = ''

$('.submit_url').click(function() {
    addLoader()
    github_repo_url = $('#search_box').val().trim()
    removeActiveClassFromPagination()
    $(".paginationpage1").addClass("active")
    getOpenIssues();
    $('.state_box').show()
    $('.pagination').show()
});

$('.open_button').click(function() {
    addLoader()
    removeActiveClassFromPagination()
    $(".paginationpage1").addClass("active")
    getOpenIssues()
})

$('.close_button').click(function() {
    addLoader()
    removeActiveClassFromPagination()
    $(".paginationpage1").addClass("active")
    getClosedIssues()
})

function addLoader() {
    $(".issuelist").html('')
    $(".issuelist").append('<div id="loading"></div>')
}

function removeActiveClassFromPagination() {
    for (i=1; i<=10; i++) {
        $(".paginationpage" + i).removeClass("active")
    }
}

function getNextPage(page) {
    addLoader()
    removeActiveClassFromPagination()
    $(".paginationpage" + page).addClass("active")

    if (pagestate == 'open') {
        getOpenIssues(page)
    }
    else {
        getClosedIssues(page)
    }
}

function getOpenIssues(page=1) {
    pagestate = 'open'
    $(".open_button").addClass("active_button")
    $(".close_button").removeClass("active_button")
    $.ajax({
        url: "/issues/open?url=" + github_repo_url + "&page=" + page,
        type: "GET",
        success: function(response) {
            $(".issuelist").html('')
            $.each(response, function(key, issue) {
                card = '<div class="issuecard">\
                            <div style="font-size: 20px;"><b>' + issue.title + '</b></div>\
                            <div style="font-size: 14px; margin-top: 3px;">opened by ' + issue.opened_by + ' on ' + issue.opened_at + '\
                        </div>'
                $(".issuelist").append(card)
            })
        },
        error: function(response) {
            alert(response.responseText)
        }
    })
}

function getClosedIssues(page=1) {
    pagestate = 'close'
    $(".close_button").addClass("active_button")
    $(".open_button").removeClass("active_button")
    $.ajax({
        url: "/issues/closed?url=" + github_repo_url + "&page=" + page,
        type: "GET",
        success: function(response) {
            $(".issuelist").html('')
            $.each(response, function(key, issue) {
                card = '<div class="issuecard">\
                            <div style="font-size: 20px;"><b>' + issue.title + '</b></div>\
                            <div style="font-size: 14px; margin-top: 3px;">closed by ' + issue.closed_by + ' on ' + issue.closed_at + '\
                        </div>'
                $(".issuelist").append(card)
            })
        },
        error: function(response) {
            alert(response.responseText)
        }
    })
}